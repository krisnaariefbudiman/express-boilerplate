const express = require("express");
const routes = require("./server/routes");
const bodyParser = require("body-parser");
const cors = require("cors");
const db = require("./server/config/db");
const socket = require("./server/socket");
const { createServer } = require("http");

const app = express();
const port = process.env.PORT || 3000;
const httpServer = createServer(app);
// create server
// socket
socket(httpServer, app);

app.use(cors());
app.use(bodyParser.json({ limit: "25mb" }));
// routes
routes(app);

db.sequelize.sync({ force: false, alter: false }).then(() => {
  console.log("connet db");
});

httpServer.listen(port, () => {
  console.log(`listening on *:${port}`);
});

module.exports = {
  apps: [
    {
      name: "express boilerplate",
      script: "app.js",
      args: "start",
      error_file: "./err.log",
      watch: ["server"],
      ignore_watch: ["logs", "node_modules", "uploads/*", "err,log"],
      env: {
        NODE_ENV: "development",
      },
      env_staging: {
        NODE_ENV: "staging",
      },
      env_production: {
        NODE_ENV: "production",
      },
    },
  ],
};

const env = require("./../config/env");
const { expressjwt: jwt } = require("express-jwt");

const jwtMiddleware = jwt({
  secret: env.authSecretKey,
  algorithms: ["HS256"],
  credentialsRequired: false,
  getToken: function fromHeaderOrQuerystring(req) {
    if (
      req.headers.authorization &&
      req.headers.authorization.split(" ")[0] === "Bearer"
    ) {
      return req.headers.authorization.split(" ")[1];
    }
    if (req.query && req.query.token) {
      return req.query.token;
    }
    return null;
  },
});

module.exports = { jwtMiddleware };

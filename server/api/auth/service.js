const env = require("./../../config/env");
const jwt = require("jsonwebtoken");

module.exports = {
  async createToken(payload) {
    // payload mush be have permission for validation at jwt permission
    const token = await jwt.sign(payload, env.authSecretKey, {
      expiresIn: env.authTokenExpired,
      algorithm: "HS256",
    });

    return token;
  },
};

const { response, ok, err } = require("./../../utils/response");

const service = require("./service");

module.exports = {
  async token(req, res) {
    try {
      // exemple payload
      const dataToken = {
        username: "admin",
        password: "1234",
        permissions: ["book_add", "book_view", "book_edit", "book_delete"],
      };
      const token = await service.createToken(dataToken);
      console.log("token ", token);
      response(token, res);
    } catch (error) {
      err(error);
    }
  },
};

module.exports = (sequelize, Sequelize) => {
  const Staff = sequelize.define(
    "Staffs",
    {
      title: {
        type: Sequelize.STRING,
      },
      description: {
        type: Sequelize.STRING,
      },
      published: {
        type: Sequelize.BOOLEAN,
      },
    },
    { timestamps: false, underscored: true }
  );
  return Staff;
};

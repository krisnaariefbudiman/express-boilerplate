const repository = require("./repository");

module.exports = {
  async createBooks(payload) {
    return await repository.createBook(payload);
  },
  async findBooks() {
    return await repository.findBook();
  },
  async findBookById(id) {
    return await repository.findBookById(id);
  },
  async updateBookById(id, payload) {
    return await repository.updateBookById(id, payload);
  },
  async deleteBookById(id) {
    return await repository.deleteBookById(id);
  },
};

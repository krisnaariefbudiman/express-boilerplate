const service = require("./service");
const { response, ok, err } = require("./../../utils/response");

module.exports = {
  async createBooks(req, res) {
    try {
      // socket
      const io = req.app.get("socketio");
      // auth
      const auth = req.auth;
      const payload = req.body;
      const data = await service.createBooks(payload);
      return response(data, res);
    } catch (error) {
      err(error, res);
    }
  },
  async findBooks(req, res) {
    try {
      // name hari
      const data = await service.findBooks();
      response(data, res);
    } catch (error) {
      err(error, res);
    }
  },
  async findBooksById(req, res) {
    try {
      // name hari
      const { id } = req.params;
      const data = await service.findBookById(id);
      response(data, res);
    } catch (error) {
      err(error, res);
    }
  },
  async updateBooksById(req, res) {
    try {
      // name hari
      const { id } = req.params;
      const body = req.body;
      const data = await service.updateBookById(id, body);
      response(data, res);
    } catch (error) {
      err(error, res);
    }
  },
  async deleteBooksById(req, res) {
    try {
      // name hari
      const { id } = req.params;
      const data = await service.deleteBookById(id);
      response(data, res);
    } catch (error) {
      err(error, res);
    }
  },
};

module.exports = (sequelize, Sequelize) => {
  const Book = sequelize.define(
    "Books",
    {
      title: {
        type: Sequelize.STRING,
      },
      description: {
        type: Sequelize.STRING,
      },
      actime: {
        type: Sequelize.DATE,
      },
      addtime: {
        type: Sequelize.DATE,
      },
      acusername: {
        type: Sequelize.STRING,
      },
      actype: {
        type: Sequelize.CHAR,
      },
    },
    { timestamps: false, underscored: true }
  );
  return Book;
};

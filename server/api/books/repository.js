const { Books } = require("./../../config/db");
const { Op } = require("sequelize");

module.exports = {
  async createBook(body) {
    const { title, description } = body;
    const payload = {
      title,
      description,
      addtime: new Date(),
      actime: new Date(),
      actype: "I",
      acusername: "sistem",
    };
    return await Books.create(payload);
  },
  async findBook() {
    return await Books.findAll({ where: { actype: { [Op.ne]: "D" } } });
  },
  async findBookById(id) {
    return await Books.findOne({ where: { id, actype: { [Op.ne]: "D" } } });
  },
  async updateBookById(id, payload) {
    return await Books.update(
      { ...payload, actype: "U", actime: new Date() },
      { where: { id, actype: { [Op.ne]: "D" } } }
    );
  },
  async deleteBookById(id) {
    return await Books.update(
      { actype: "D", actime: new Date() },
      { where: { id, actype: { [Op.ne]: "D" } } }
    );
  },
};

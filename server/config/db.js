const dbConfig = require("./db.config");
const { Sequelize } = require("sequelize");
const sequelize = new Sequelize(
  dbConfig.database,
  dbConfig.username,
  dbConfig.password,
  {
    host: dbConfig.host,
    dialect: dbConfig.dialect,
    operationsAliases: false,
  }
);
const db = {};
const Op = Sequelize.Op;

db.Sequelize = Sequelize;
db.sequelize = sequelize;
// model data
db.Staffs = require("./../api/staffs/model")(sequelize, Sequelize, Op);
db.Books = require("./../api/books/model")(sequelize, Sequelize, Op);

module.exports = db;

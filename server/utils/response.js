const error = (err, res, status) => {
  res.status(status || 500).json({
    meta: {
      status: status || 500,
      message: typeof err === "string" ? err : err.message,
    },
    data: null,
    errors: [
      {
        code: typeof err === "string" ? err : err.code,
      },
    ],
  });
  res.end();
};

const ok = (rows, res) => {
  res.status(200).json({
    meta: {
      status: 200,
    },
    data: rows,
    errors: null,
  });
  res.end();
};

const response = (rows, res) => {
  try {
    ok(rows, res);
  } catch (err) {
    error(err, res);
  }
};

exports.err = error;
exports.ok = ok;
exports.response = response;

const v1Routes = require("./v1");
const v2Routes = require("./v2");
const { jwtMiddleware } = require("./../middleware/jwt");

const routes = (app) => {
  app.use(jwtMiddleware);
  app.use("/api/v1", v1Routes);
  app.use("/api/v2", v2Routes);
};

module.exports = routes;

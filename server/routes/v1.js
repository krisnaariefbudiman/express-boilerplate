const express = require("express");
const guard = require("express-jwt-permissions")({ requestProperty: "auth" });
const api = require("./../api");

const router = express.Router();

router.get("/", (req, res) => {
  res.send("v1 routes");
});
// token
router.post("/token", api.auth.token);
// books
router.post("/books", guard.check("book_add"), api.books.createBooks);
router.get("/books", api.books.findBooks);
router.get("/books/:id", api.books.findBooksById);
router.put("/books/:id", api.books.updateBooksById);
router.delete("/books/:id", api.books.deleteBooksById);

module.exports = router;

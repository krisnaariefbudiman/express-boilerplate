const { Server } = require("socket.io");

const socket = (httpServer, app) => {
  // create socket io object

  const io = new Server(httpServer, {
    /* options */
  });
  console.log("set item ");
  app.set("socketio", io);

  io.on("connection", (socket) => {
    // ...
    console.log("socket cek");

    socket.on("userConnected", (val) => {
      socket.join(val);
    });

    socket.on("subscribe", (room) => {
      try {
        socket.join(room);
        console.log(`user ${socket.id} has join the ${room}`);
      } catch (error) {
        console.log("socket error ", error);
      }
    });

    socket.on("unsubscribe", (room) => {
      try {
        socket.leave(room, () => {
          console.log(`user ${socket.id} has left the${room}`);
        });
      } catch (error) {
        console.log("socket error ", error);
      }
    });

    socket.on("unsubscribeAll", () => {
      try {
        socket.leaveAll();
      } catch (error) {
        console.log("socket error ", error);
      }
    });
  });
};

module.exports = socket;

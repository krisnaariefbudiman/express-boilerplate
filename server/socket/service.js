module.exports = {
  async emitToClient(io, data) {
    io.emit(data);
  },
};
